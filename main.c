#include<stdio.h>
#include<string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/msg.h>
#include <stdlib.h>

#define MSG_KEY         0x9867
typedef struct msg {
    long type;
    char *file_path;
}msg_t;

#define MSG_SZ (sizeof(msg_t)-sizeof(long))

 int main (void)
 {
     int ret;
     int fd[2];
     int err,s;
     int mqid;
     char str[10];
     char file[32]=".c";
     ret=pipe(fd);
     mqid=msgget(MSG_KEY, IPC_CREAT| 0777);
     if(mqid==-1)
     {
         printf("mssget failed\n");
         exit(1);
     }

     ret=fork();
     if( ret==0)
     {  
         //read process
         close(fd[1]);
         dup2(fd[0],STDIN_FILENO);
         close(fd[0]);//close write

         msg_t mg1;
            printf("enter a file\n");
            gets(str); // accpeted the file path
           mg1.file_path=strcat(str,file);
           char *args[]={"vim",file,NULL};
           err=execvp("vim",args);
           if(err==0)
           {
               printf("faield to open\n");
               exit(1);
           }
           mg1.type=21;
        ret= msgsnd(mqid,&mg1,MSG_SZ,0);
        close(fd[0]);
     }
     else
     {
        close(fd[0]);
        dup2(fd[1],STDOUT_FILENO);
        close(fd[1]);//Lclose rwrite
        err=execlp("ls -l","ls -l",NULL);
        if (err<0)
         {
             printf("faild to send metadata\n")2;
         }
         msg_t mg2;
         printf("parent :  takeing from child");
         ret=msgrcv(mqid,&mg2,MSG_SZ,21,0);
         printf("parent recved:     %s\n",mg2.file_path);
         
         msgctl(mqid,IPC_RMID,NULL);
         close(fd[1]);
         wait(ret &s,0);
     }
     return 0;
 }